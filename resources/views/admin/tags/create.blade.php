@extends('layouts.app')
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card">
        <div class="card-header">
            Create New Tag
        </div>
        <div class="card-header">
            <form action="{{route('tag.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="tag">Name</label>
                    <input type="text" name="tag" class="form-control" value="{{old('tag')}}">

                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit">Store Tag</button>
                    </div>
                </div>


            </form>
        </div>
    </div>

@endsection