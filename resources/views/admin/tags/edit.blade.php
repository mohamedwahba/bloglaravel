@extends('layouts.app')
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card">
        <div class="card-header">
            Edit Tag {{$tags->tag}}
        </div>
        <div class="card-header">
            <form action="{{route('tag.update',['id'=>$tags->id])}}" method="post">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="tag">Tag Name</label>
                    <input type="text" name="tag" class="form-control" value="{{$tags->tag}}">

                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit">Store Tag</button>
                    </div>
                </div>


            </form>
        </div>
    </div>

@endsection