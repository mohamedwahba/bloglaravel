@extends('layouts.app')
@section('content')


    <div class="card">
        <div class="card-header">ALL Tags</div>

        <div class="card-body">
            <table class="table table-hover">
                <tr>
                    <th>Name</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                <tbody>
                @if($tags->count() > 0)
                    @foreach($tags as $tag)
                        <tr>
                            <td>{{$tag->tag}}</td>
                            <td>
                                <a href="{{route('tag.edit',['id'=>$tag->id])}}" class="btn btn-info">Edit</a>

                            </td>
                            <td>
                                <form action="{{route('tag.destroy',['id'=>$tag->id])}}" method="post">
                                    <input type="submit" value="Delete" class="btn btn-danger">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </td>
                        </tr>
                    @endforeach

                @else
                    <tr>
                        <th colspan="5" class="text-center">No Tags</th>
                    </tr>
                @endif


                </tbody>
            </table>
        </div>
    </div>



@endsection