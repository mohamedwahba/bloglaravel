@extends('layouts.app')
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card">
        <div class="card-header">
            Edit User
        </div>
        <div class="card-header">
            <form action="{{route('change.update',['id'=>$user->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('patch')
                <div class="form-group">
                    <label for="tag">Name</label>
                    <input type="text" name="name" class="form-control" value="{{$user->name}}">

                </div>

                <div class="form-group">
                     <label for="tag">email</label>
                    <input type="email" name="email" class="form-control" value="{{$user->email}}">

                </div>


                <div class="form-group">
                    <label for="password">password</label>
                    <input type="password" name="password" class="form-control" value="">

                </div>

                <div class="form-group">
                    <label for="avatar">upload your avatar</label>
                    <input type="file" name="avatar" class="form-control" value="">

                </div>

                <div class="form-group">
                    <label for="about">about</label>
                    <textarea class="form-control" name="about"  cols="6" rows="6">{{$user->profile->about}}</textarea>
                </div>

                <div class="form-group">
                    <label for="facebook">facebook</label>
                    <input type="text" name="facebook" class="form-control" value="{{$user->profile->facebook}}">

                </div>

                <div class="form-group">
                    <label for="youtube">youtube</label>
                    <input type="text" name="youtube" class="form-control" value="{{$user->profile->youtube}}">

                </div>

                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit">Update Profile</button>
                    </div>
                </div>


            </form>
        </div>
    </div>

@endsection