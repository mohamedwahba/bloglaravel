@extends('layouts.app')
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card">
        <div class="card-header">
            Create New User
        </div>
        <div class="card-header">
            <form action="{{route('users.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="tag">Name</label>
                    <input type="text" name="name" class="form-control" value="{{old('name')}}">

                </div>

                <div class="form-group">
                    <label for="email">email</label>
                    <input type="email" name="email" class="form-control" value="{{old('email')}}">

                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit">Store User</button>
                    </div>
                </div>


            </form>
        </div>
    </div>

@endsection