@extends('layouts.app')
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card">
        <div class="card-header">
            Update Post
        </div>
        <div class="card-header">
            <form action="{{route('posts.update',['id'=>$posts->id])}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" value="{{$posts->title}}">

                </div>

                <div class="form-group">
                    <label for="featured">Featured image</label>
                    <input type="file" name="featured" class="form-control" value="">

                </div>

                <label for="tag">Select Tag</label>
                @foreach($tags as $tag)
                    <div class="form-check">

                        <label><input type="checkbox" class="form-check-input" value="{{$tag->id}}" name="tags[]"
                                      @foreach($posts->tags as $t)
                                      @if($t->id == $tag->id)
                                      checked
                                    @endif
                                    @endforeach
                            >{{$tag->tag}}</label>
                    </div>
                @endforeach


                <div class="form-group">
                    <label for="category">Select Category</label>
                    <select name="category_id" class="form-control">
                        @foreach($category as $cat)
                            <option value="{{$cat->id}}"
                                    @if($posts->category_id == $cat->id)
                                    selected
                                    @endif

                            >{{$cat->name}}</option>
                        @endforeach


                    </select>

                </div>


                <div class="form-group">
                    <label for="content">Content</label>
                    <textarea name="content" id="content" cols="5" rows="5"
                              class="form-control">{{$posts->content}}</textarea>

                </div>

                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit">Store Post</button>
                    </div>
                </div>


            </form>
        </div>
    </div>

@endsection