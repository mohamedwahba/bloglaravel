@extends('layouts.app')
@section('content')


    <div class="card">

        <div class="card-body">
            <table class="table table-hover">
                <tr>
                    <th>image</th>
                    <th>title</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                <tbody>
                @if($all_posts->count() > 0)

                @foreach($all_posts as $post)
                    <tr>
                        <td><img src="{{$post->featured}}" width="80px" height="80px"></td>
                        <td>{{$post->title}}</td>
                        <td><a href="{{route('posts.edit',['id'=>$post->id])}}" class="btn btn-success">Edit</a></td>
                        <td>
                            <form action="{{route('posts.destroy',['id'=>$post->id])}}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="Delete" class="btn btn-danger">
                            </form>
                        </td>
                    </tr>
                @endforeach
                @else
                    <tr>
                        <th colspan="5" class="text-center">No Posts</th>
                    </tr>
                @endif


                </tbody>
            </table>
        </div>
    </div>



@endsection