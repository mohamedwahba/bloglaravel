@extends('layouts.app')
@section('content')


    <div class="card">

        <div class="card-body">
            <table class="table table-hover">
                <tr>
                    <th>image</th>
                    <th>title</th>
                    <th>Edit</th>
                    <th>Restore</th>
                    <th>Destroy</th>
                </tr>
                <tbody>
                @if($trahsed->count() >0)
                    @foreach($trahsed as $post)
                        <tr>
                            <td><img src="{{$post->featured}}" width="80px" height="80px"></td>
                            <td>{{$post->title}}</td>
                            <td>edit</td>
                            <td><a href="{{route('posts.restore',['id'=>$post->id])}}"
                                   class="btn btn-success">Restore</a></td>
                            <td>
                                <form action="{{route('posts.kill',['id'=>$post->id])}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="Delete" class="btn btn-danger">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    @else
                    <tr>
                        <th colspan="5" class="text-center">No Trashed Post</th>
                    </tr>
                @endif

                </tbody>
            </table>
        </div>
    </div>



@endsection