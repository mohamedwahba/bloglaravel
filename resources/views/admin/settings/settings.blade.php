@extends('layouts.app')
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card">
        <div class="card-header">
            Edit Blog Settings
        </div>
        <div class="card-header">
            <form action="{{route('settings.update')}}" method="post">
                @csrf
                @method('PATCH')
                <div class="form-group">
                    <label for="site_name">Sit Name</label>
                    <input type="text" name="site_name" class="form-control" value="{{$site->site_name}}">

                </div>

                <div class="form-group">
                    <label for="contact_number">Sit Contacts</label>
                    <input type="text" name="contact_number" class="form-control" value="{{$site->contact_number}}">

                </div>

                <div class="form-group">
                    <label for="contact_email">Sit Email</label>
                    <input type="email" name="contact_email" class="form-control" value="{{$site->contact_email}}">

                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" name="address" class="form-control" value="{{$site->address}}">

                </div>

                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit">Update Site </button>
                    </div>
                </div>


            </form>
        </div>
    </div>

@endsection