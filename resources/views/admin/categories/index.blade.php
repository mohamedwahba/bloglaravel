@extends('layouts.app')
@section('content')


    <div class="card">

        <div class="card-body">
            <table class="table table-hover">
                <tr>
                    <th>Name</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                <tbody>
                @if($categories->count() > 0)
                @foreach($categories as $category)
                    <tr>
                        <td>{{$category->name}}</td>
                        <td>
                            <a href="{{route('category.edit',['id'=>$category->id])}}" class="btn btn-info">Edit</a>

                        </td>
                        <td>
                            <form action="{{route('category.destroy',['id'=>$category->id])}}" method="post">
                                <input type="submit" value="Delete" class="btn btn-danger">
                                @csrf
                                @method('DELETE')
                            </form>
                        </td>
                    </tr>
                @endforeach

                @else
                    <tr>
                        <th colspan="5" class="text-center">No Category</th>
                    </tr>
                @endif


                </tbody>
            </table>
        </div>
    </div>



@endsection