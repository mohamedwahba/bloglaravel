<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Settings;
use App\Tag;
use Illuminate\Http\Request;

class frontendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome')->with('title', Settings::first()->site_name)
            ->with('category', Category::take(5)->get())
            ->with('first_post', Post::orderBy('created_at', 'desc')->first())
            ->with('second_post', Post::orderBy('created_at', 'desc')->skip(1)->take(1)->first())
            ->with('third_post', Post::orderBy('created_at', 'desc')->skip(2)->take(1)->first())
            ->with('settings', Settings::first());


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function single($slug)
    {
        $post = Post::where('slug', $slug)->first();
//        $post = Post::find(2);

        $next_id = Post::where('id', '>', $post->id)->min('id');
        $prev_id = Post::where('id', '<', $post->id)->max('id');


        return view('single')
            ->with('post', $post)
            ->with('title', $post->title)
            ->with('category', Category::take(5)->get())
            ->with('settings', Settings::first())
            ->with('next', Post::find($next_id))
            ->with('prev', Post::find($prev_id))
            ->with('tags', Tag::all());

    }

    public function category($id)
    {
        $category = Category::find($id);
        return view('category')
            ->with('categories',$category)
            ->with('title',$category->title)
            ->with('settings', Settings::first())
            ->with('category', Category::take(5)->get())
            ;
    }


}
