<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;

class postsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_posts = Post::all();
        return view('admin.posts.index', compact('all_posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();

        if ($category->count() == 0) {
            session()->flash('info', 'you must have one category');
            return redirect()->back();
        }
        $tags = Tag::all();

        if ($tags->count() == 0) {
            session()->flash('info', 'you must have one tag');
            return redirect()->back();
        }


        return view('admin.posts.create', compact('category'))->with('tags', Tag::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'featured' => 'required|image',
            'content' => 'required',
            'category_id' => 'required',
            'tags' => 'required'
        ]);

        $featured = $request->featured;
        $newFeatured = time() . $featured->getClientOriginalName();

        $featured->move('uploads', $newFeatured);

        $post = Post::create([
            'title' => $request->title,
            'featured' => 'uploads/' . $newFeatured,
            'content' => \request('content'),
            'category_id' => $request->category_id,
            'slug' => str_slug($request->title),
            'user_id'=>auth()->user()->id,
        ]);

        $post->tags()->attach($request->tags);


        session()->flash('success', 'Post created successful');

        return redirect()->route('home');


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        $category = Category::all();
        return view('admin.posts.edit', ['posts' => $post, 'category' => $category])->with('tags', Tag::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'category_id' => 'required',
        ]);

        $post = Post::find($id);

        if ($request->hasFile('featured')) {
            $featured = $request->featured;
            $feturead_new_name = time() . $featured->getClientOriginalName();
            $featured->move('uploads', $feturead_new_name);
            $post->featured = 'uploads/' . $feturead_new_name;
        }

        $post->update([
            'title' => $request->title,
            'content' => \request('content'),
            'category_id' => $request->category_id,
        ]);

        $post->tags()->sync($request->tags);

        session()->flash('success', 'Post update success');
        return redirect()->route('posts.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::find($id)->delete();
        session()->flash('success', 'Post trashed successfuly');
        return back();
    }

    public function trashed()
    {
        $trashed = Post::onlyTrashed()->get();

        return view('admin.posts.trashed', ['trahsed' => $trashed]);

    }

    public function kill($id)
    {
        $trashed = Post::withTrashed()->where('id', $id)->first();
        $trashed->forceDelete();
        session()->flash('success', 'Post Deleted successfuly');
        return back();

    }

    public function restore($id)
    {
        $restore = Post::withTrashed()->where('id', $id)->first();

        $restore->restore();
        session()->flash('success', 'Post restored successfuly');

        return redirect()->route('posts.index');
    }


}
