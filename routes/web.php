<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Category;
use App\Settings;

Route::get('/', 'frontendController@index');
//

Route::post('/email',function (){
    \Illuminate\Support\Facades\Mail::to('test@test.com')->send(new \App\Mail\contactEmail());
    session()->flash('success','Your email sent');
    return redirect()->back();

});

Auth::routes();

Route::get('/category/{id}', 'frontendController@category')->name('category.single');

Route::get('/search/result', function () {
    $post = \App\Post::where('title', 'like', '%' . request('query') . '%')->get();
    return view('results')->with('posts', $post)
        ->with('category', Category::take(5)->get())
        ->with('title', 'Search Result: '.request('query'))
        ->with('settings', Settings::first())
        ->with('query',request('query'));

});


Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::resource('posts', 'postsController');
    Route::resource('category', 'categoriesController');
    Route::get('/trashed', 'postsController@trashed')->name('posts.trashed');
    Route::delete('/kill/{id}', 'postsController@kill')->name('posts.kill');
    Route::get('/restore/{id}', 'postsController@restore')->name('posts.restore');

    Route::resource('tag', 'tagsController');
    Route::resource('/users', 'UsersController');
    Route::get('/users/admin/{id}', 'UsersController@makeAdmin')->name('users.admin');
    Route::get('/users/admin/user/{id}', 'UsersController@makeNotAdmin')->name('users.user');

    Route::resource('/profiles/change', 'profileController');
    Route::get('/site/settings', 'SettingsController@edit')->name('settings.edit');
    Route::patch('/site/settings/update', 'SettingsController@update')->name('settings.update');

    Route::get('/home', 'HomeController@index')->name('home');



});

Route::get('/single/{id}', 'frontendController@single')->name('single.slug.index');

Route::get('/category/view/{id}','frontendController@category')->name('category.view');
