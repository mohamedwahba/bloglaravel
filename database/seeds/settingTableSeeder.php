<?php

use Illuminate\Database\Seeder;

class settingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Settings::create([
            'site_name' => 'wahba blog',
            'contact_number' =>'01066243716',
            'contact_email' =>'wahba207930000@gmaol.com',
            'address' => 'Zagazig Egypt'
        ]);
    }
}
