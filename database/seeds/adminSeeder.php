<?php

use Illuminate\Database\Seeder;
use App\User;
use App\profile;

class adminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('123456789'),
            'admin' => 1,
        ]);

        profile::create([
            'user_id' =>$user->id,
            'avatar' => 'avatars/1.JPG',
            'about' =>'test test test ',
            'facebook'=>'facebook.com',
            'youtube'=>'youtube.com',
        ]);


    }
}
